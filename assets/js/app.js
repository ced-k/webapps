// Helpers
// @ -- prepros-prepend ../../node_modules/lodash/core.min.js

// STORE (vuex)
// @prepros-prepend ./_store.js

// vue-resource config
Vue.http.options.root = '/';
Vue.http.options.emulateJSON = true; // nécessaire pour envoyer un objet en POST
Vue.http.options.headers = {
	'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
};



// TODO: text input drop URL document.getElementById('elementid').onfocus
// TODO: save app list on github (ask for auth, create gist)



// APP
const app = new Vue ({

	el: '#app',
	store: store,

	http: {
		root: '/',
		headers: {}
	},

	data: {
		jets: null, // Jets search engine
		appListComputed: false, // Controls Jets refresh
		appListVersion: '',

		searchTerms: '',

		menuExpanded: false,
		panels: {
			newapp: {
				expanded: false,
				more: false,
			}
		},

		newApp: {
			window: {
				width: 1024,
				height: 600,
			}
		}
	},

	computed: {
		appList: {
			get: function () {
				this.appListComputed = true;
				// return _.orderBy (store.getters.appList, 'name')
				console.log(store.getters.appList);
				return store.getters.appList;
			},
			set: function (newValue) {
				store.commit ('saveAppList', newValue);
			}
		},

		isDetached: function () {
			let windowCheck = (window.opener || window.top !== window.self);
			console.log(windowCheck);
			return windowCheck;
		}
	},


	watch: {
		appListComputed: function (val) {
			if (val === true) {
				this.setupSearchEngine ();
				this.setAppsIcons ();
				this.appListComputed = false;
			}
		},
	},


	created () {
		// load app list
		store.dispatch ('loadAppList');
	},


	mounted () {
		this.setAppsIcons ();
		this.setupSearchEngine ();
	},


	methods: {

		// open webapps in popup
		detachWebapps () {
			let args = 'width=460,height=640,toolbar=0,location=0,menubar=0,left=208,top=150';
			return window.open ('/', 'webapps', args);
		},

		// remove app
		removeApp (index) {
			let appList = JSON.parse (JSON.stringify (this.appList));
			appList.splice (index, 1);
			this.appList = appList;
		},


		// search tag
		searchTag (tag) {
			document.querySelector ('[data-search-input]').value = tag;
			this.jets.search ();
		},


		// Init Jets.js
		setupSearchEngine () {
			this.jets = new Jets({
				searchTag: '[data-search-input]',
				contentTag: '[data-search-content]',
				searchSelector: '*OR',
				callSearchManually: false,
				diacriticsMap: {
					a: 'ÀÁÂÃÄÅàáâãäåĀāąĄ',
					c: 'ÇçćĆčČ',
					d: 'đĐďĎ',
					e: 'ÈÉÊËèéêëěĚĒēęĘ',
					i: 'ÌÍÎÏìíîïĪī',
					l: 'łŁ',
					n: 'ÑñňŇńŃ',
					o: 'ÒÓÔÕÕÖØòóôõöøŌō',
					r: 'řŘ',
					s: 'ŠšśŚ',
					t: 'ťŤ',
					u: 'ÙÚÛÜùúûüůŮŪū',
					y: 'ŸÿýÝ',
					z: 'ŽžżŻźŹ'
				},
				manualContentHandling: function (tag) {

					let content = '';
					if (! tag) content = '';
					let a = tag.querySelector ('a');

					if (tag.innerText) content = tag.innerText;
					if (tag.textContent) content = tag.textContent;

					if (a.getAttribute('data-category')) content += ' ' + a.getAttribute('data-category');

					return content;
				}
			});
		},


		// new app sumbission
		onSubmitApp (event) {
			if (Object.keys(this.newApp).length === 0) return false;

			// clone into newApp
			let newApp = JSON.parse (JSON.stringify (this.newApp));

			console.log(this.newApp);

			// setup tags array
			newApp.tags = newApp.tags ? newApp.tags.split(' ') : [];

			// add new app
			this.appList.push (newApp);
			store.commit ('saveAppList', this.appList);
		},


		// apps icons
		setAppsIcons: function () {

			for (let x in this.appList) {
				let app = this.appList[x];
				let a = document.createElement ('a');
				a.href = app.url;
				let domain = a.hostname;
				let iconUrl = `https://api.statvoo.com/favicon/?url=${domain}`;
				// let iconUrl = `https://www.google.com/s2/favicons?domain=${domain}`;
				// let apiURL = `http://favicon.yandex.net/favicon/${domain}`;

				this.appList[x].icon = iconUrl;

				// this.$http.get(apiURL).then (response => {
				// 	console.log(response.body);
				// 	// Vue.set(state.appList, appListApp[index], response.body);
				// }, response => {
				// 	console.error ('loadAppList', response)
				// });
			}

		},


		openApp: function (event) {

			event.preventDefault ();

			console.log('event.target', event.target);

			if (! event.target.closest('.data-app-button, .data-app-button *')) return false;
			let target = event.target.closest('.data-app-button');

			// get app options
			let appID = target.getAttribute('data-index');
			let appOptions = this.appList[appID];
			appOptions.window = appOptions.window || {};

			// merge with defaults
			let defaultWindow = store.getters.getDefaultWindow;
			let options = Object.assign ({}, defaultWindow, appOptions.window);

			// center window
			options.left = ((screen.width / 2) - (options.width / 2));
			options.top = ((screen.height / 2) - (options.height / 2));

			// translate to window.open args
			let args = [];
			for (let [key, val] of Object.entries(options)) args.push (`${key}=${val}`);
			args = args.join(',');

			// open app
			console.log ('win args', args, appOptions);
			return window.open (appOptions.url, appOptions.name, args);
		},



		// https://css-tricks.com/favicons-next-to-external-links/
		_getFavicon: function (url) {
			// let domain = url.match(/:\/\/(.[^/]+)/)[1];
			let a = document.createElement ('a');
			a.href = url;
			let domain = a.hostname;
			// let apiURL = `https://www.google.com/s2/favicons?domain=${domain}`;
			let apiURL = `https://api.statvoo.com/favicon/?url=${domain}`;
			return apiURL;
		},


		tagList: function (value) {
			if (typeof value === 'undefined') return value;

			let links = [];
			let terms = value.split(' ');

			terms.forEach (term => {
				let linkTpl = `<button class="button-tag">${term}</button>`;
				links.push (linkTpl);
			});

			return links.join (' ');
		}

	},




});
