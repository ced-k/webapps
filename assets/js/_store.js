// STORE

const stringToHash = (s) => {
	let hash = 0, i, chr;
	if (s.length === 0) return hash;
	for (let i = 0; i < s.length; i++) {
		chr   = s.charCodeAt(i);
		hash  = ((hash << 5) - hash) + chr;
		hash |= 0; // Convert to 32bit integer
	}
	return hash;
};



const store = new Vuex.Store({

	state: {

		defaultWindow: {
			width: 1024,
			height: 600,
			toolbar: 0,
			location: 0,
			menubar: 0,
		},

		appList : [],
		appListVersion: '',
	},


	mutations: {
		initState (state) {
		},


		// saveAppList
		saveAppList (state, appList) {
			let appListString = JSON.stringify (appList);
			let appListVersion = stringToHash (appListString);
			state.appList = appList;
			state.appListVersion = stringToHash (appListString);
			localStorage.setItem ('appList', appListString);
			localStorage.setItem ('appListVersion', appListVersion);
		},


		// load app list from storage
		loadAppList (state, appList) {

			for (let x in appList) {
				let app = appList[x];
				let a = document.createElement ('a');
				a.href = app.url;
				let domain = a.hostname;
				let iconUrl = `https://api.statvoo.com/favicon/?url=${domain}`;
				// let iconUrl = `https://www.google.com/s2/favicons?domain=${domain}`;
				// let iconUrl = `http://favicon.yandex.net/favicon/${domain}`;
				appList[x].icon = iconUrl;

				// tags
				if (typeof app.category !== 'undefined') {
					appList[x].tags = app.category;
				}
			}

			let appListString = JSON.stringify (appList);
			state.appList = appList;
			state.appListVersion = stringToHash (appListString);

			// overwrite localStorage
			localStorage.setItem ('appList', appListString);
			localStorage.setItem ('appListVersion', state.appListVersion);
		},

	},



	getters: {
		appList (state) {
			// TODO: load from localStorage
			return state.appList;
		},
		getDefaultWindow (state) {
			return state.defaultWindow;
		}
	},


	actions: {
		loadAppList (context) {
			console.log ('loadAppList action');

			let appList = localStorage.getItem ('appList');

			// load from local storage
			if (appList !== null) {
				let appListJSON = JSON.parse (localStorage.getItem ('appList'));
				context.commit ('loadAppList', appListJSON);
			}
			else {
				// load default data
				Vue.http.get('data/applist.json').then (response => {
					context.commit ('loadAppList', response.body);
				}, response => {
					console.error ('loadAppList', response)
				});
			}
		}
	}

});
